import 'package:flutter/material.dart';
import 'package:flutter_test_counter/screens/counter_screen.dart';

import 'package:flutter_test_counter/screens/counter_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: CounterScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}
