import 'package:flutter/material.dart';
import 'package:flutter_test_counter/wigets/float_add_minus_and_reset_buttons.dart';

class CounterScreen extends StatefulWidget {
  const CounterScreen({super.key});

  @override
  State<CounterScreen> createState() => _CounterScreenState();
}

class _CounterScreenState extends State<CounterScreen> {
  var counter = 0;

  void _decrease () {
    setState(() => counter--);
  }

  void _increase () {
    setState(() => counter++);
  }

  void _reset () {
    setState(() => counter = 0);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('HomeScreen'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'Counter',
              style: TextStyle(fontSize: 16),
            ),
            Text('$counter'),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatAddMinusAndResetButtons(
        increase: _increase,
        decrease: _decrease,
        reset: _reset,
      )
    );
  }
}
