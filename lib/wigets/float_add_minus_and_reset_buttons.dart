import 'dart:ffi';

import 'package:flutter/material.dart';

class FloatAddMinusAndResetButtons extends StatelessWidget {
  final Function increase;
  final Function decrease;
  final Function reset;
  final double spaceBetweenButtons;

  const FloatAddMinusAndResetButtons(
      {super.key,
      required this.increase,
      required this.decrease,
      required this.reset,
      this.spaceBetweenButtons = 20.0});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FloatingActionButton(
            onPressed: () => increase(),
            child: const Icon(Icons.add),
          ),
          SizedBox(
            width: spaceBetweenButtons,
          ),
          FloatingActionButton(
            onPressed: () => decrease(),
            child: const Icon(Icons.remove_sharp),
          ),
          SizedBox(
            width: spaceBetweenButtons,
          ),
          FloatingActionButton(
            onPressed: () => reset(),
            child: const Icon(Icons.restart_alt_rounded),
          ),
        ],
      ),
    );
  }
}
